<?php

require __DIR__ . '/vendor/autoload.php';

use Vin\ShopwareSdk\Data\Context;
use Vin\ShopwareSdk\Data\Filter\EqualsFilter;
use Vin\ShopwareSdk\Data\Filter\NotFilter;
use Vin\ShopwareSdk\Service\SyncService;
use Vin\ShopwareSdk\Service\Struct\SyncPayload;
use Vin\ShopwareSdk\Data\Entity\Product\ProductDefinition;
use Vin\ShopwareSdk\Service\Struct\SyncOperator;
use Vin\ShopwareSdk\Factory\RepositoryFactory;
use Vin\ShopwareSdk\Data\Criteria;

class SyncServiceExample {
    public function execute(): void
    {
        require __DIR__ . '/token.php';

        // Config Section
        // if simulation is true, the script will not sync to shopware
        $simulation = false;
        // if this is true, the script will stop after the first batch
        $endAfterFirstBatch = false;
        // batch size how many products to sync in one batch
        $batchSize = 50;
        // sleep seconds between each batch
        $sleepSeconds = 1;
        // cms page id to set for the products
        $cmsPageId = 'YOUR_CMS_PAGE_ID';
        // End of config section

        $context = new Context($config['shop_url'], $accessToken);
        $syncService = new SyncService($context);

        while (true) {
            $productCriteria = new Criteria();
            $productCriteria->setLimit($batchSize);
            $productCriteria->addFilter(new NotFilter(NotFilter::OP_AND, [new EqualsFilter('cmsPageId', $cmsPageId)]));
            $productCriteria->addFilter(new EqualsFilter('parentId', null));

            $productRepository = RepositoryFactory::create(ProductDefinition::ENTITY_NAME);
            $result = $productRepository->search($productCriteria, $context);

            $syncPayloadData = [];

            foreach ($result->getEntities()->getIterator() as $productEntity) {
                $productId = $productEntity->getProperty('id');
                $productName = $productEntity->getProperty('name');
                $currentCmsPageId = $productEntity->getProperty('cmsPageId');
                $parentId = $productEntity->getProperty('parentId');

                echo("productId: " . $productId . "\n");
                echo("productName: " . $productName . "\n");
                echo("parentId: " . $parentId . "\n");
                echo("currentCmsPageId: " . $currentCmsPageId . "\n");

                if($currentCmsPageId === $cmsPageId) {
                    echo "Skip. Already set desired cmsPageId\n";
                    continue;
                }

                if($parentId) {
                    echo "Skip. Product is child product\n";
                    continue;
                }

                $syncPayloadData[] = ['id' => $productId, 'cmsPageId' => $cmsPageId];
            }



            var_dump($syncPayloadData);

            if($simulation) {
                echo("Skip sync due to simulation mode\n");
                return;
            }

            if(count($syncPayloadData) === 0) {
                echo("No product remaining to sync\n");
                return;
            }

            printf("============================================\n");
            printf("Sync response:\n");
            $payload = new SyncPayload();
            $payload->set(ProductDefinition::ENTITY_NAME . '-upsert', new SyncOperator(ProductDefinition::ENTITY_NAME, SyncOperator::UPSERT_OPERATOR,
                $syncPayloadData
            ));
            $response = $syncService->sync($payload);
            if(200 === $response->getStatusCode()) {
                printf("Sync success\n");
            } else {
                printf("Sync failed\n");
            }

            if($endAfterFirstBatch) {
                printf("endAfterFirstBatch\n");
                return;
            }

            if($sleepSeconds > 0) {
                sleep($sleepSeconds);
            }
        }
    }
}

$example = new SyncServiceExample();
$example->execute();