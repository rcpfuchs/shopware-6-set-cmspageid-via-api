# Shopware 6 - Set custom product layout for all products
# Shopware 6 - Ein Produktlayout für alle Produkte festlegen

## Disclaimer

Please run this code only if you can understand what it does. I do not take any responsibility for any damage caused by this code. This is just a quick and dirty solution to set a custom product layout for all products in Shopware 6.

## Need help? / Hilfe benötigt?

If you need someone to run this code for you, feel free to contact me on https://www.theiss.me

# How to use / Anwendung

Edit the file `index.php` and change the value of the variable `$cmsPageId` to the ID of the layout you want to set for all products. You can find the id in the url when you edit the layout in the admin panel.

Then edit the `aut-config.json` file and set the correct values for `shop_url`, `client_id` and `client_secret`.

Clone this repository and run the following command in the root directory of the repository:

```composer install```

After that, you can run the following command to set the custom product layout for all products:

```php index.php```

Pro Tip: You can edit the variables in the Config section in `index.php` to savely test the script with 1 product first. Then step up and maybe run 50 products in each batch.

# License

You can use the code if you want. 

# Thanks
Thanks to the vin-sw/shopware-sdk for the Shopware SDK. 
