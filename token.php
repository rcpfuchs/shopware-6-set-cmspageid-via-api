<?php

require __DIR__ . '/vendor/autoload.php';

use \Vin\ShopwareSdk\Client\GrantType\PasswordGrantType;
use \Vin\ShopwareSdk\Client\GrantType\ClientCredentialsGrantType;
use \Vin\ShopwareSdk\Client\AdminAuthenticator;

$config = json_decode(file_get_contents(__DIR__ . '/auth-config.json'), true);
//$grantType = new PasswordGrantType($config['username'], $config['password']);
//$adminClient = new AdminAuthenticator($grantType, $config['shop_url']);
//$accessToken = $adminClient->fetchAccessToken();

//$config = $this->getConfig();
$grantType = new ClientCredentialsGrantType($config['client_id'], $config['client_secret']);
$adminClient = new AdminAuthenticator($grantType, $config['shop_url']);
$accessToken = $adminClient->fetchAccessToken();

//var_dump("============================================");
//var_dump("Fetch access token using client credential grant type: \n");
//var_dump($accessToken);

//return $accessToken;